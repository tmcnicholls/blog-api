<?php

/**
 * @group api
 * @group api.post
 * 
 */
class PostTest extends APITester {

	/**
	 * @var mixed
	 * @access public
	 */
	protected $_model = 'Post';
	
	/**
	 * @var string
	 * @access protected
	 */
	protected $_apiPath = 'v1/posts';
	
	public function test_it_returns_ok()
	{
		$this->getJson($this->_apiPath, 'GET');
		$this->assertResponseOk();
	}
		
	public function test_it_creates_a_post_given_valid_input()
    {
        // Post valid creation data from our object stub
        $this->getJson('v1/posts', 'POST', $this->getStub());
        // Assert we get a 201 created response
        $this->assertResponseStatus(201);
    }

    public function test_it_throws_400_error_if_validation_fails()
    {
        // Post invalid creation data to our posts creation endpoint
        $this->getJson('v1/posts', 'POST');
        // Assert we get a 400 invalid request response
        $this->assertResponseStatus(400);
    }
	
	protected function getStub()
	{
		return[
			'title' => $this->fake->sentence,
			'content' => $this->fake->paragraph,
			'user_id' => '1',
		];
	}

}