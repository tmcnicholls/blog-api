<?php

use Faker\Factory as Faker;

abstract class APITester extends TestCase {

    /**
     * @var int
     */
    protected $times = 1;

    /**
     * @var Faker\Generator
     */
    protected $fake;

    /**
     *
     */
    function __construct($name = NULL, array $data = array(), $dataName = '')
	{
		$this->fake = Faker::create();
		return parent::__construct($name, $data, $dataName);
	}


    /**
     *
     */
    public function setUp()
	{
		parent::setUp();
 
		$this->app['artisan']->call('migrate');
		$this->app['artisan']->call('db:seed');
	}


    /**
     *
     */
    public function tearDown()
	{
		$this->times = 1;
	}


    /**
     * @param $count
     * @return $this
     */
    protected function times($count)
	{
		$this->times = $count;
 
		return $this;
	}


    /**
     * @param $uri
     * @param string $method
     * @param array $parameters
     * @return mixed
     */
    protected function getJson($uri, $method = 'GET', $parameters = [])
	{
		return json_decode($this->call($method, $uri, $parameters)->getContent());
	}


    /**
     * @param $type
     * @param array $fields
     */
    protected function make($type, array $stub = [], array $fields = [])
	{
	
		$stub = ( empty($stub) ? $this->getStub() : $stub );
	
		while ($this->times--)
		{
			$stub = array_merge($stub, $fields);
			
			Eloquent::unguard();
			
			$type::create($stub);
		}
	}


    /**
     * @return array
     */
    protected function getStub()
	{
		return [];
	}


    /**
     *
     */
    protected function assertObjectHasAttributes()
	{
		$args = func_get_args();
		$object = array_shift($args);
 
		foreach ($args as $attribute)
		{
			$this->assertObjectHasAttribute($attribute, $object);
		}
	}
	

}