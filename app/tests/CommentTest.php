<?php

/**
 * @group api
 * @group api.comment
 * 
 */
class CommentTest extends APITester {

	/**
	 * @var mixed
	 * @access public
	 */
	protected $_model = 'Comment';
	
	/**
	 * @var string
	 * @access protected
	 */
	protected $_apiPath = 'v1/comments';
	
	public function test_it_returns_ok()
	{
		$this->getJson($this->_apiPath, 'GET');
		$this->assertResponseOk();
	}
	
	public function test_it_creates_a_post_given_valid_input()
    {
        // Post valid creation data from our object stub
        $this->getJson($this->_apiPath, 'POST', $this->getStub());
        // Assert we get a 201 created response
        $this->assertResponseStatus(201);
    }

    public function test_it_throws_400_error_if_validation_fails()
    {
        // Post invalid creation data to our creation endpoint
        $this->getJson($this->_apiPath, 'POST');
        // Assert we get a 400 invalid request response
        $this->assertResponseStatus(400);
    }
	
	protected function getStub()
	{
		return[
			'comment' => $this->fake->paragraph,
			'user_id' => '1',
			'post_id' => '1'
		];
	}
	

}