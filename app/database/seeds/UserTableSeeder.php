<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		// Create test user with known password
		User::create([
			'email' => 'test@mcnicholls.uk',
			'password' => Hash::make('testing')
		]);
		
		// Create random test users
		foreach(range(1, 10) as $index)
		{
			User::create([
				'email' => $faker->email(),
				'password' => Hash::make($faker->word()),
			]);
		}
	}

}