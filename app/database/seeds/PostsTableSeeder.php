<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		
		$userIDs = User::lists('id');

		foreach(range(1, 10) as $index)
		{
			Post::create([
				'title' => $faker->sentence(4),
				'content' => $faker->paragraph(2),
				'user_id' => $faker->randomElement($userIDs),
				'approved' => $faker->boolean(),
			]);
		}
	}

}