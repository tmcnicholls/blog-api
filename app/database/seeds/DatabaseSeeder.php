<?php

class DatabaseSeeder extends Seeder {

	/**
     * @var array
     */
    private $tables = [
        'users',
        'posts',
        'comments',
    ];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Prepare database for seeding
		$this->cleanDatabase();
	
		Eloquent::unguard();
		
		$this->call('UserTableSeeder');
		$this->call('PostsTableSeeder');
		$this->call('CommentsTableSeeder');
	}
	
	/**
     *
     */
    private function cleanDatabase()
    {
        // Disable foreign key checks so we can truncate any pivot tables before seeding data
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        foreach($this->tables as $tableName)
        {
            DB::table($tableName)->truncate();
        }
		
		// Enable foreign key checks for usual database operation
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
