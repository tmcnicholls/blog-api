<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class CommentsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
		
		$userIDs = User::lists('id');
		$postIDs = Post::lists('id');

		foreach(range(1, 50) as $index)
		{
			Comment::create([
				'post_id' => $faker->randomElement($postIDs),
				'user_id' => $faker->randomElement($userIDs),
				'comment' => $faker->paragraph(2),
				'approved' => $faker->boolean(),
			]);
		}
	}

}