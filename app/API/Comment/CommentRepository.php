<?php namespace API\Comment;

use Comment;
use API\DbRepository;
use Illuminate\Http;
use Input;
use Request;
use Auth;
use API\Services\Validation\CommentValidator as Validator;

class CommentRepository extends DbRepository implements CommentRepositoryInterface {

	/**
	 * @var Spot
	 */
	protected $model;
	
	/**
	* @var \API\Services\Validation\CommentValidator
	*/
	protected $validator;
	
	/**
	 * @param Event $model
	 */
	function __construct(Comment $model, Validator $validator)
	{
		$this->model = $model;
		$this->validator = $validator;
	}
	
	/**
	* @param array $data
	* @return \Response
	* @throws ValidationException
	* @throws \Exception
	*/
	public function createEntry(array $data)
	{		
		try {
			
			if ( ! $this->validator->isValidForCreation($data) ):
				return $this->errorWrongArgs( $this->validator->errors() );
			endif;
			
		} catch (ValidationException $e) {
		
			throw $e;
		}
		 
		$entry = Comment::create( $data );

		return $this->setStatusCode(201)->respondWithArray([
			'comment_id' => (int) $entry->id,
		]);
	}
	
	/**
	* Get all approved comments
	*
	* @param null
	* @return mixed
	*/
	public function getApprovedComments()
	{
		return $this->model->where('approved', '=', '1')->get();
	}	
	
}