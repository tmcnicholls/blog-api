<?php namespace API\User;

use User;
use API\DbRepository;
use Illuminate\Http;
use Input;
use Request;
use Auth;
use API\Services\Validation\UserValidator as Validator;

class UserRepository extends DbRepository implements UserRepositoryInterface {

	/**
	 * @var Spot
	 */
	protected $model;
	
	/**
	* @var \API\Services\Validation\UserValidator
	*/
	protected $validator;
	
	/**
	 * @param Event $model
	 */
	function __construct(User $model, Validator $validator)
	{
		$this->model = $model;
		$this->validator = $validator;
	}
	
	/**
	* @param array $data
	* @return \Response
	* @throws ValidationException
	* @throws \Exception
	*/
	public function create(array $data)
	{		
		try {
			
			if ( ! $this->validator->isValidForCreation($data) ):
				return $this->errorWrongArgs( $this->validator->errors() );
			endif;
			
		} catch (ValidationException $e) {
		
			throw $e;
		}
		 
		$entry = User::create( $data );

		return $this->respondWithArray([
			'user_id' => (int) $entry->user_id,
		]);
	}
	
}