<?php namespace API\Services\Validation;

class CommentValidator extends Validator {

    /**
     * @var array
     */
	static $insertRules = [
		'post_id' => 'required|integer',
		'user_id' => 'required|integer',
		'comment' => 'required',
	];

    /**
     * @var array
     */
    static $updateRules = [
    
	];
	
}