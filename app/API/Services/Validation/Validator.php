<?php namespace API\Services\Validation;

use Validator as Valid;

abstract class Validator {

 
    /**
     * @var
     */
    protected $errors;
	protected $error_keys;


    /**
     * @param $validator
     * @return bool
     */
    public function check($validator)
	{
		if ( $validator->fails() ) {
			$this->errors = $validator->messages();
			$this->error_keys = $validator->failed();
			return false;
		}
		return true;
	}

    /**
     * @param $input
     * @return bool
     */
    public function isValidForCreation($input)
	{
		$validator = Valid::make($input, static::$insertRules);		
		
		return $this->check($validator);
	}

    /**
     * @param $input
     * @return bool
     */
    public function isValidForUpdate($input)
	{
		$validator = Valid::make($input, static::$updateRules);
		return $this->check($validator);
	}


    /**
     * @return mixed
     */
    public function errors()
	{
		return $this->errors->getMessages();
	}
 
	/**
     * @return mixed
     */
	public function error_keys()
	{
		return $this->error_keys;
	}
}
