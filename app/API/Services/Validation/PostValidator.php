<?php namespace API\Services\Validation;

class PostValidator extends Validator {

    /**
     * @var array
     */
	static $insertRules = [
		'user_id' => 'required|integer',
		'title' => 'required',
		'content' => 'required',
	];

    /**
     * @var array
     */
    static $updateRules = [
    
	];
	
}