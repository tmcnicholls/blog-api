<?php namespace API\Services\Validation;

class UserValidator extends Validator {

    /**
     * @var array
     */
	static $insertRules = [
		'email' => 'required|email',
		'password' => 'required|min:6',
	];

    /**
     * @var array
     */
    static $updateRules = [
    
	];
	
}