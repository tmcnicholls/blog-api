<?php namespace API\Post;

use Post;
use API\DbRepository;
use Illuminate\Http;
use Input;
use Request;
use Auth;
use API\Services\Validation\PostValidator as Validator;

class PostRepository extends DbRepository implements PostRepositoryInterface {

	/**
	 * @var Spot
	 */
	protected $model;
	
	/**
	* @var \API\Services\Validation\PostValidator
	*/
	protected $validator;
	
	/**
	 * @param Event $model
	 */
	function __construct(Post $model, Validator $validator)
	{
		$this->model = $model;
		$this->validator = $validator;
	}
	
	/**
	* @param array $data
	* @return \Response
	* @throws ValidationException
	* @throws \Exception
	*/
	public function createEntry(array $data)
	{		
		try {
			
			if ( ! $this->validator->isValidForCreation($data) ):
				return $this->errorWrongArgs( $this->validator->errors() );
			endif;
			
		} catch (ValidationException $e) {
		
			throw $e;
		}
		 
		$entry = Post::create( $data );

		return $this->setStatusCode(201)->respondWithArray([
			'post_id' => (int) $entry->id,
		]);
	}
	
	/**
	* Get all approved posts
	*
	* @param null
	* @return mixed
	*/
	public function getApprovedPosts()
	{
		return $this->model->where('approved', '=', '1')->get();
	}	
	
}