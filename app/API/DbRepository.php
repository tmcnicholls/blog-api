<?php namespace API;

abstract class DbRepository extends \APIController {

	/**
	 * Eloquent model
	 */
	protected $model;
	protected $transformer;
		
	/**
	 * @param $model
	 */
	function __construct($model, $transformer)
	{	
		$this->model = $model;
		$this->transformer = $transformer;
	}

	/**
	 * Fetch a record by id
	 *
	 * @param $id
	 * @return mixed
	 */
	public function getByID($id)
	{
		return $this->model->find($id);
	}
	
	/**
	* Fetch all records
	*
	* @param null
	* @return mixed
	*/
	public function getAll()
	{
		return $this->model->paginate();
	}
		
    /**
     * Searching all records
     * @param $query
     * @return mixed
     */
    public function search($query)
	{
		if ( is_array( $query ) ):
		
			$columns = $this->getColumns();
			
			foreach($query as $key => $value):
				if ( ! in_array($key, $this->restricted_keywords() ) && in_array($key, $columns) ):
					$this->model = $this->model->where($key, "=", $value);
				endif;
	        endforeach;				
		endif;
        return $this->model->paginate();
	}
	
	/**
	 * getColumns function.
	 * 
	 * @access public
	 * @return mixed
	 */
	public function getColumns()
	{
		 if( $data = $this->model->first() ):
		 	return array_keys($data->toArray());
		 else:
		 	return false;
		 endif;
	}
	
	/**
    * @param $id
    * @return mixed
    */
	public function delete($id)
	{
		if ( $this->model->where("id", "=", $id)->delete() ):
			return $this->respondWithSuccess("Deleted" .$id);
		endif;
	}
		

} 