<?php namespace API\Transformer\User;

use User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

	protected $availableEmbeds = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [         
            'user_id' 	  => (string) $user->user_id,
			'username'    => (string) $user->email,
        ];
    }
	
}