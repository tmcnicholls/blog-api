<?php namespace API\Transformer\Post;

use Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract {

	protected $availableEmbeds = [
		'comments'
    ];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Post $post)
    {
        return [         
            'title'		=> (string) $post->title,
			'content'   => (string) $post->content,
        ];
    }
    
	 /**
     * Embed Detals
     *
     * @return League\Fractal\Resource\Collection
     */
	public function embedComments(Post $post)
	{
	    if( $comments = $post->comments ):

	    	return $this->collection($comments, new \API\Transformer\Comment\CommentTransformer);

	    endif;
	}

	
}