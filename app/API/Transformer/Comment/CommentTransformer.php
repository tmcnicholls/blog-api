<?php namespace API\Transformer\Comment;

use Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract {

	protected $availableEmbeds = [];

    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(Comment $comment)
    {
        return [         
            'user_id'		=> $comment->user_id,
            'user_email'	=> $comment->user->email,
			'comment'		=> (string) $comment->comment,
			'approved'		=> (bool) $comment->approved,
        ];
    }
	
}