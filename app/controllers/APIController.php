<?php

use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Manager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class APIController extends Controller
{
	protected $statusCode = 200;
	
	public function __construct(Manager $fractal)
	{	 	
		$this->fractal = $fractal;
						
		// Are we going to try and include embedded data?
		$this->fractal->setRequestedScopes(explode(',', Input::get('embed')));				
	}
		
	/**
	 * Getter for statusCode
	 *
	 * @return mixed
	 */
	public function getStatusCode()
	{
		return $this->statusCode;
	}
	
	/**
	 * Setter for statusCode
	 *
	 * @param int $statusCode Value to set
	 *
	 * @return self
	 */
	public function setStatusCode($statusCode)
	{
		$this->statusCode = $statusCode;
		return $this;
	}
	
	protected function respondWithItem($item, $callback)
	{
		$resource = new Item($item, $callback);

		$rootScope = $this->fractal->createData($resource);

		return $this->respondWithArray($rootScope->toArray());
	}

	protected function respondWithCollection($collection, $callback)
	{
		$resource = new Collection($collection, $callback);

		$rootScope = $this->fractal->createData($resource);
		
		return $this->respondWithArray($rootScope->toArray());
	}

	protected function respondWithArray(array $array, array $headers = [])
	{
		$response = Response::json($array, $this->statusCode, $headers);

		// $response->header('Content-Type', 'application/json');

		return $response;
	}
	
	 /**
	 * Respond with an array or traversable, and a transformer.
	 *
	 * @param mixed $item Data to be wrapped with League\Fractal\Resource\Item
	 * @param callable|League\Fractal\Resource\ResourceInterface $item
	 *
	 * @return	Response
	 */
	protected function respondWithPaginator(Paginator $paginator, $transformer)
	{
		$rawCollection = $paginator->getCollection();

		$resource = new Collection($rawCollection, $transformer);

		$resource->setPaginator(new IlluminatePaginatorAdapter($paginator));
		
		return $this->fractal->createData($resource)->toArray();
	}
	
	protected function respondWithError($message)
	{
		if ($this->statusCode === 200) {
			trigger_error(
				"You better have a really good reason for erroring on a 200...",
				E_USER_WARNING
			);
		}

		return $this->respondWithArray([
			'error' => [
				'http_code' => $this->statusCode,
				'message' => $message,
			]
		]);
	}
	
	protected function respondWithSuccess($message)
	{
		 return $this->respondWithArray([
			'data' => [
				'http_code' => $this->statusCode,
				'message' => $message,
			]
		]);
	}
		
	/**
	 * Generates a Response with a 403 HTTP header and a given message.
	 *
	 * @return	Response
	 */
	public function errorForbidden($message = 'Forbidden')
	{
		return $this->setStatusCode(403)->respondWithError($message);
	}

	/**
	 * Generates a Response with a 500 HTTP header and a given message.
	 *
	 * @return	Response
	 */
	public function errorInternalError($message = 'Internal Error')
	{
		return $this->setStatusCode(500)->respondWithError($message);
	}
	
	/**
	 * Generates a Response with a 404 HTTP header and a given message.
	 *
	 * @return	Response
	 */
	public function errorNotFound($message = 'Resource Not Found')
	{
		return $this->setStatusCode(404)->respondWithError($message);
	}

	/**
	 * Generates a Response with a 401 HTTP header and a given message.
	 *
	 * @return	Response
	 */
	public function errorUnauthorized($message = 'Unauthorized')
	{
		return $this->setStatusCode(401)->respondWithError($message);
	}

	/**
	 * Generates a Response with a 400 HTTP header and a given message.
	 *
	 * @return	Response
	 */
	public function errorWrongArgs($message = 'Wrong Arguments')
	{
		return $this->setStatusCode(400)->respondWithError($message);
	}
	
	public function to_slug($string){
    	return strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
	}
	
	public static function restricted_keywords()
	{
		return [
			"embed",
			"page",
			"limit",
			"start"
		];
	}


}
