<?php

use API\Comment\CommentRepository as CommentRepository;
use API\Transformer\Comment\CommentTransformer;

class CommentController extends \APIController {

	/**
	 * @var CommentRepository
	 */
	private $comment_repo;
	
	/**
	 * @param CommentRepository $comment_repo
	 */
	function __construct(CommentRepository $comment_repo)
	{
		$fractal = new League\Fractal\Manager;
		parent::__construct($fractal);
					
		$this->comment_repo = $comment_repo;
	}
	
	/**
	 * Store a newly created resource in storage.
	 * POST /v1/comments
	 *
	 * @return Response
	 */
	public function store()
	{
		return $this->comment_repo->createEntry(Input::all());
	}
	
	/**
	 * Retrieve approved comments
	 * GET /v1/comments
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = $this->comment_repo->getApprovedComments();

		return $this->respondWithCollection($data, new CommentTransformer);		
	}

}