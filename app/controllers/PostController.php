<?php

use API\Post\PostRepository as PostRepository;
use API\Transformer\Post\PostTransformer;

class PostController extends \APIController {

	/**
	 * @var PostRepository
	 */
	private $post_repo;
	
	/**
	 * @param PostRepository $post_repo
	 */
	function __construct(PostRepository $post_repo)
	{
		$fractal = new League\Fractal\Manager;
		parent::__construct($fractal);
					
		$this->post_repo = $post_repo;
	}
	
	/**
	 * Store a newly created resource in storage.
	 * POST /v1/posts
	 *
	 * @return Response
	 */
	public function store()
	{
		return $this->post_repo->createEntry(Input::all());
	}
	
	/**
	 * Retrieve approved posts
	 * GET /v1/posts
	 *
	 * @return Response
	 */
	public function index()
	{
		$data = $this->post_repo->getApprovedPosts();

		return $this->respondWithCollection($data, new PostTransformer);		
	}

}