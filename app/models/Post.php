<?php

class Post extends Eloquent {

	protected $fillable = [
		'user_id',
		'title',
		'content',
	];

    public function comments()
    {
        return $this->hasMany('Comment');
    }

}