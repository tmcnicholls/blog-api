<!DOCTYPE html><html><head><meta charset="utf-8"><title>Blog API</title><link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"><link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"><link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,700|Inconsolata|Raleway:200"><style>/* Highlight.js Theme Tomorrow */
.hljs-comment,.hljs-title{color:#8e908c}.hljs-variable,.hljs-attribute,.hljs-tag,.hljs-regexp,.ruby .hljs-constant,.xml .hljs-tag .hljs-title,.xml .hljs-pi,.xml .hljs-doctype,.html .hljs-doctype,.css .hljs-id,.css .hljs-class,.css .hljs-pseudo{color:#c82829}.hljs-number,.hljs-preprocessor,.hljs-pragma,.hljs-built_in,.hljs-literal,.hljs-params,.hljs-constant{color:#f5871f}.ruby .hljs-class .hljs-title,.css .hljs-rules .hljs-attribute{color:#eab700}.hljs-string,.hljs-value,.hljs-inheritance,.hljs-header,.ruby .hljs-symbol,.xml .hljs-cdata{color:#718c00}.css .hljs-hexcolor{color:#3e999f}.hljs-function,.python .hljs-decorator,.python .hljs-title,.ruby .hljs-function .hljs-title,.ruby .hljs-title .hljs-keyword,.perl .hljs-sub,.javascript .hljs-title,.coffeescript .hljs-title{color:#4271ae}.hljs-keyword,.javascript .hljs-function{color:#8959a8}.hljs{display:block;background:white;color:#4d4d4c;padding:.5em}.coffeescript .javascript,.javascript .xml,.tex .hljs-formula,.xml .javascript,.xml .vbscript,.xml .css,.xml .hljs-cdata{opacity:.5}</style><style>body,
h4,
h5 {
  font-family: 'Roboto' sans-serif !important;
}
h1,
h2,
h3,
.aglio {
  font-family: 'Raleway' sans-serif !important;
}
h1 a,
h2 a,
h3 a,
h4 a,
h5 a {
  display: none;
}
h1:hover a,
h2:hover a,
h3:hover a,
h4:hover a,
h5:hover a {
  display: inline;
}
code {
  color: #444;
  background-color: #ddd;
  font-family: 'Inconsolata' monospace !important;
}
a[data-target] {
  cursor: pointer;
}
h4 {
  font-size: 100%;
  font-weight: bold;
  text-transform: uppercase;
}
.back-to-top {
  position: fixed;
  z-index: 1;
  bottom: 0px;
  right: 24px;
  padding: 4px 8px;
  background-color: #eee;
  text-decoration: none !important;
  border-top: 1px solid rgba(0,0,0,0.1);
  border-left: 1px solid rgba(0,0,0,0.1);
  border-right: 1px solid rgba(0,0,0,0.1);
  border-top-left-radius: 3px;
  border-top-right-radius: 3px;
}
.panel {
  overflow: hidden;
}
.panel-heading code {
  margin-left: 3px;
  color: #c7254e;
  background-color: rgba(255,255,255,0.7);
  white-space: pre-wrap;
  white-space: -moz-pre-wrap;
  white-space: -pre-wrap;
  white-space: -o-pre-wrap;
  word-wrap: break-word;
}
.panel-heading h3 {
  margin-top: 10px;
  margin-bottom: 10px;
}
a.list-group-item:hover {
  background-color: #f8f8f8;
  border-left: 2px solid #555;
  padding-left: 15px;
}
.indent {
  display: block;
  text-indent: 16px;
}
.list-group-item {
  padding-left: 16px;
}
.list-group-item .toggle .open {
  display: block;
}
.list-group-item .toggle .closed {
  display: none;
}
.list-group-item.collapsed .toggle .open {
  display: none;
}
.list-group-item.collapsed .toggle .closed {
  display: block;
}
a.list-group-item {
  font-size: 13px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
}
a.list-group-item.heading {
  font-size: 15px;
  background-color: #f5f5f5;
}
a.list-group-item.heading:hover {
  background-color: #f8f8f8;
}
.list-group-item.collapse {
  display: none;
}
.list-group-item.collapse.in {
  display: block;
}
.list-group-item a span.closed {
  display: none;
}
.list-group-item a span.open {
  display: block;
}
.list-group-item a.collapsed span.closed {
  display: block;
}
.list-group-item a.collapsed span.open {
  display: none;
}
#nav {
  width: inherit;
  margin-top: 38px;
  max-width: 255px;
  top: 0;
  bottom: 0;
  padding-right: 12px;
  padding-bottom: 12px;
  overflow-y: auto;
}
@media( max-width: 1199px) {
  #nav {
    max-width: 212px;
  }
}
</style></head><body><a href="#top" class="text-muted back-to-top"><i class="fa fa-toggle-up"></i>&nbsp;Back to top</a><div class="container"><div class="row"><div class="col-md-3"><nav id="nav" class="hidden-sm hidden-xs affix nav"><div class="list-group"><a data-toggle="" data-target="#package-details-menu" href="#package-details" class="list-group-item heading collapsed">Package Details</a><div id="package-details-menu"><a href="#package-details-installation" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item">Installation</a></div></div><div class="list-group"><a data-toggle="" data-target="#api-details-menu" href="#api-details" class="list-group-item heading collapsed">API Details</a><div id="api-details-menu"><a href="#api-details-api-description" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item">API Description</a><a href="#api-details-api-location" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item">API Location</a></div></div><div class="list-group"><a data-toggle="" data-target="#content-items-menu" href="#content-items" class="list-group-item heading collapsed">Content Items</a><div id="content-items-menu"></div></div><div class="list-group"><a data-toggle="" data-target="#posts-menu" href="#posts" class="list-group-item heading collapsed">Posts</a><div id="posts-menu"><a href="#posts-get-posts" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item"><span class="badge alert-info"><i class="fa fa-arrow-down"></i></span>Get Posts</a><a href="#posts-create-post" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item"><span class="badge alert-success"><i class="fa fa-plus"></i></span>Create Post</a></div></div><div class="list-group"><a data-toggle="" data-target="#comments-menu" href="#comments" class="list-group-item heading collapsed">Comments</a><div id="comments-menu"><a href="#comments-get-comments" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item"><span class="badge alert-info"><i class="fa fa-arrow-down"></i></span>Get Comments</a><a href="#comments-get-post-comments" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item"><span class="badge alert-info"><i class="fa fa-arrow-down"></i></span>Get Post Comments</a><a href="#comments-create-comment" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item"><span class="badge alert-success"><i class="fa fa-plus"></i></span>Create Comment</a></div></div><div class="list-group"><a data-toggle="" data-target="#changelog-menu" href="#changelog" class="list-group-item heading collapsed">Changelog</a><div id="changelog-menu"><a href="#changelog-document-v1.0" style="border-top-left-radius: 0; border-top-right-radius: 0" class="list-group-item">Document v1.0</a></div></div><p style="text-align: center; word-wrap: break-word;"><a href="http://api.mcnicholls.uk">http://api.mcnicholls.uk</a></p></nav></div><div class="col-md-8"><div><header><div class="page-header"><h1 id="top">Blog API</h1></div></header><div class="description"><p>Please note: this document is a <strong>work in progress</strong>.</p>
<p>Subsequent releases of this document will be accompanied by a changelog.</p>
</div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="package-details">Package Details&nbsp;<a href="#package-details"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><h4 id="package-details-installation">Installation&nbsp;<a href="#package-details-installation"><i class="fa fa-link"></i></a></h4><p>This API package is based on Laravel 4. It can be cloned from the following Git repository:</p>
<p><a href="https://tmcnicholls@bitbucket.org/tmcnicholls/blog-api.git">https://tmcnicholls@bitbucket.org/tmcnicholls/blog-api.git</a></p>
<p>Once the repo has been cloned, you may need to make the <code>app/storage</code> directory writable by the web server.</p>
<p>In order to install third party packages, please run a <code>composer update</code></p>
<p>The database configuration can be found at <code>app/config/database.php</code></p>
<p>Once a database has been created, run <code>php artisan migrate --seed</code> to run the migrations and seeders to populate the API with sample data.</p>
</div></div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="api-details">API Details&nbsp;<a href="#api-details"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><h4 id="api-details-api-description">API Description&nbsp;<a href="#api-details-api-description"><i class="fa fa-link"></i></a></h4><p>This sample API provides access to a REST API for retrieving and modifying Blog content. Currently it is possible to create and retrieve Posts and Comments.</p>
<p>Retrieving comments for a post is also possible via the use of embeds.</p>
<p>A request to the API must start with the special hook in the following format:</p>
<p><code>/&lt;version&gt;</code> where <code>&lt;version&gt;</code> is a number of the service version.</p>
<p>The current version of the API is v1, therefore the hook is <code>/v1</code>.</p>
<h4 id="api-details-api-location">API Location&nbsp;<a href="#api-details-api-location"><i class="fa fa-link"></i></a></h4><p>The API currently lives at the following URL:</p>
<ul>
<li><strong>Development</strong> - <a href="http://api.mcnicholls.uk">http://api.mcnicholls.uk</a></li>
</ul>
</div></div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="content-items">Content Items&nbsp;<a href="#content-items"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><p>The API contains the following content types:</p>
<ul>
<li>Post - <em>A blog post object</em></li>
<li>Comment - <em>Comments added to a post</em></li>
</ul>
</div></div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="posts">Posts&nbsp;<a href="#posts"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><h4 id="posts-get-posts">Get Posts&nbsp;<a href="#posts-get-posts"><i class="fa fa-link"></i></a></h4><section id="posts-get-posts-get" class="panel panel-info"><div class="panel-heading"><div style="float:right"><span style="text-transform: lowercase">Get Posts</span></div><div style="float:left"><a href="#posts-get-posts-get" class="btn btn-xs btn-primary">GET</a></div><div style="overflow:hidden"><code>/v1/posts/</code></div></div><div class="panel-body"><p>Retrieve a list of all approved posts.</p>
</div><ul class="list-group"><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>200</code></strong><a data-toggle="collapse" data-target="#1117c24087081d76af1b75f4466c89d5" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="1117c24087081d76af1b75f4466c89d5" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Cache-Control</span>: <span class="hljs-string">no-cache</span><br><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>    [
        {
          "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
            {
              "<span class="hljs-attribute">title</span>": <span class="hljs-value"><span class="hljs-string">"Asperiores architecto ad cumque."</span></span>,
              "<span class="hljs-attribute">content</span>": <span class="hljs-value"><span class="hljs-string">"Vel voluptates eaque qui et atque laudantium. Praesentium fugit itaque temporibus aut quibusdam rerum est. Sunt possimus temporibus sunt asperiores aut."</span>
            </span>},
            {
              "<span class="hljs-attribute">title</span>": <span class="hljs-value"><span class="hljs-string">"Nulla ut eum accusantium atque."</span></span>,
              "<span class="hljs-attribute">content</span>": <span class="hljs-value"><span class="hljs-string">"Voluptatem nostrum fugiat et cumque eveniet. Reprehenderit necessitatibus et autem et quae voluptas. At aliquid nulla maxime ipsa hic dicta delectus."</span>
            </span>},
            {
              "<span class="hljs-attribute">title</span>": <span class="hljs-value"><span class="hljs-string">"Saepe eius temporibus architecto voluptatibus."</span></span>,
              "<span class="hljs-attribute">content</span>": <span class="hljs-value"><span class="hljs-string">"Culpa error ex mollitia unde ut pariatur sit. Ab aut nesciunt inventore deserunt dolor. Voluptatem eaque veritatis saepe."</span>
            </span>},
            {
              "<span class="hljs-attribute">title</span>": <span class="hljs-value"><span class="hljs-string">"Placeat ut earum laboriosam dolore eius."</span></span>,
              "<span class="hljs-attribute">content</span>": <span class="hljs-value"><span class="hljs-string">"Similique rerum doloribus veniam. Quia consequatur dolore tenetur ab odio corporis id totam."</span>
            </span>}
          ]</span>,
          "<span class="hljs-attribute">embeds</span>": <span class="hljs-value">[
            <span class="hljs-string">"comments"</span>
          ]
        </span>}
    ]
</code></pre></li></ul></section><h4 id="posts-create-post">Create Post&nbsp;<a href="#posts-create-post"><i class="fa fa-link"></i></a></h4><section id="posts-create-post-post" class="panel panel-success"><div class="panel-heading"><div style="float:right"><span style="text-transform: lowercase">Create Post</span></div><div style="float:left"><a href="#posts-create-post-post" class="btn btn-xs btn-success">POST</a></div><div style="overflow:hidden"><code>/v1/posts/</code></div></div><div class="panel-body"><p>Create a post</p>
</div><ul class="list-group"><li class="list-group-item bg-default"><strong>Parameters</strong></li><li class="list-group-item">
<dl class="dl-horizontal">
	<dt>user_id</dt>
	<dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>1</span></span><p>User ID</p></dd>
	<dt>title</dt>
	<dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>Test Post</span></span><p>Post title</p></dd>
	<dt>content</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>Vel voluptates eaque qui...</span></span><p>Post content</p></dd>
</dl>
</li></ul><ul class="list-group"><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>201</code></strong><a data-toggle="collapse" data-target="#7ff936a9f803237a480597d687d434b1" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="7ff936a9f803237a480597d687d434b1" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Cache-Control</span>: <span class="hljs-string">no-cache</span><br><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>    [
        {
            "<span class="hljs-attribute">post_id</span>": <span class="hljs-value"><span class="hljs-number">11</span>
        </span>}
    ]            
</code></pre></li><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>400</code></strong><a data-toggle="collapse" data-target="#9bd911381076e323472df56e546d0d16" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="9bd911381076e323472df56e546d0d16" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">http_code</span>": <span class="hljs-value"><span class="hljs-number">400</span></span>,
    "<span class="hljs-attribute">message</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">title</span>": <span class="hljs-value">[
        <span class="hljs-string">"The missing field is required."</span>
      ]
    </span>}
  </span>}
</span>}
</code></pre></li></ul></section></div></div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="comments">Comments&nbsp;<a href="#comments"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><h4 id="comments-get-comments">Get Comments&nbsp;<a href="#comments-get-comments"><i class="fa fa-link"></i></a></h4><section id="comments-get-comments-get" class="panel panel-info"><div class="panel-heading"><div style="float:right"><span style="text-transform: lowercase">Get Comments</span></div><div style="float:left"><a href="#comments-get-comments-get" class="btn btn-xs btn-primary">GET</a></div><div style="overflow:hidden"><code>/v1/comments/</code></div></div><div class="panel-body"><p>Retrieve all approved comments.</p>
</div><ul class="list-group"><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>200</code></strong><a data-toggle="collapse" data-target="#4a29017174fe14779cc2155d6061e681" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="4a29017174fe14779cc2155d6061e681" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Cache-Control</span>: <span class="hljs-string">no-cache</span><br><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>    [
        {
          "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
            {
              "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"1"</span></span>,
              "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"test@mcnicholls.uk"</span></span>,
              "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Incidunt aut consectetur qui aut ea nobis minima. Consequuntur excepturi iusto quia culpa et et molestiae."</span></span>,
              "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
            </span>},
            {
              "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"2"</span></span>,
              "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"dkunze@miller.biz"</span></span>,
              "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Sunt sunt voluptatem libero assumenda labore. Facere natus qui quo autem sit iste."</span></span>,
              "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
            </span>},
            {
              "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"7"</span></span>,
              "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"barrows.benton@leannonconroy.com"</span></span>,
              "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Iste vitae et ut iusto voluptates. Eos natus dolorum velit eaque doloribus et quis iure."</span></span>,
              "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
            </span>},
            {
              "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"9"</span></span>,
              "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"edna.hermiston@bernhardleffler.com"</span></span>,
              "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Aut itaque possimus repellat culpa architecto eos. Aspernatur distinctio ad saepe rem quidem autem."</span></span>,
              "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
            </span>}
          ]
        </span>}
    ]
</code></pre></li></ul></section><h4 id="comments-get-post-comments">Get Post Comments&nbsp;<a href="#comments-get-post-comments"><i class="fa fa-link"></i></a></h4><section id="comments-get-post-comments-get" class="panel panel-info"><div class="panel-heading"><div style="float:right"><span style="text-transform: lowercase">Get post comments</span></div><div style="float:left"><a href="#comments-get-post-comments-get" class="btn btn-xs btn-primary">GET</a></div><div style="overflow:hidden"><code>/v1/posts?embed=comments</code></div></div><div class="panel-body"><p>Retrieve comments for posts</p>
</div><ul class="list-group"><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>200</code></strong><a data-toggle="collapse" data-target="#dc82390a14b4c62087e32545a1ebe4aa" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="dc82390a14b4c62087e32545a1ebe4aa" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Cache-Control</span>: <span class="hljs-string">no-cache</span><br><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>    [
        {
          "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
            {
              "<span class="hljs-attribute">title</span>": <span class="hljs-value"><span class="hljs-string">"Asperiores architecto ad cumque."</span></span>,
              "<span class="hljs-attribute">content</span>": <span class="hljs-value"><span class="hljs-string">"Vel voluptates eaque qui et atque laudantium. Praesentium fugit itaque temporibus aut quibusdam rerum est. Sunt possimus temporibus sunt asperiores aut."</span></span>,
              "<span class="hljs-attribute">comments</span>": <span class="hljs-value">{
                "<span class="hljs-attribute">data</span>": <span class="hljs-value">[
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"6"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"veronica86@hotmail.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Ut quia maiores placeat qui. Maxime sint reiciendis quae. Ut in ad et laborum impedit ut dolorum."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"6"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"veronica86@hotmail.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Id laboriosam suscipit et non. Dolor sed deleniti adipisci aut voluptates nulla. Architecto aspernatur explicabo ea autem quidem porro."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"4"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"okey.friesen@hotmail.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Quis cumque soluta qui qui. Reprehenderit est aut ea sunt animi."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"6"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"veronica86@hotmail.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Quos ut aperiam et. Assumenda sint perferendis omnis deserunt consequatur assumenda."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"7"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"barrows.benton@leannonconroy.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Rerum voluptatem at dolorem reiciendis quis ut ipsum. Atque eum magnam totam vel."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">false</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"2"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"dkunze@miller.biz"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Sit amet sit nisi libero illum. Maxime id reiciendis delectus quas cum."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
                  </span>},
                  {
                    "<span class="hljs-attribute">user_id</span>": <span class="hljs-value"><span class="hljs-string">"7"</span></span>,
                    "<span class="hljs-attribute">user_email</span>": <span class="hljs-value"><span class="hljs-string">"barrows.benton@leannonconroy.com"</span></span>,
                    "<span class="hljs-attribute">comment</span>": <span class="hljs-value"><span class="hljs-string">"Consequatur laudantium cumque non aut at praesentium aperiam. Cupiditate aut illo pariatur nisi rerum. Dolores aliquid neque quam quos."</span></span>,
                    "<span class="hljs-attribute">approved</span>": <span class="hljs-value"><span class="hljs-literal">true</span>
                  </span>}
                ]
              </span>}
            </span>}
          ]
        </span>}
    ]
</code></pre></li></ul></section><h4 id="comments-create-comment">Create Comment&nbsp;<a href="#comments-create-comment"><i class="fa fa-link"></i></a></h4><section id="comments-create-comment-post" class="panel panel-success"><div class="panel-heading"><div style="float:right"><span style="text-transform: lowercase">Create Comment</span></div><div style="float:left"><a href="#comments-create-comment-post" class="btn btn-xs btn-success">POST</a></div><div style="overflow:hidden"><code>/v1/comments/</code></div></div><div class="panel-body"><p>Create a comment</p>
</div><ul class="list-group"><li class="list-group-item bg-default"><strong>Parameters</strong></li><li class="list-group-item">
<dl class="dl-horizontal">
	<dt>post_id</dt>
	<dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>10</span></span><p>Post ID</p></dd>
	<dt>user_id</dt><dd><code>integer</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>1</span></span><p>User ID</p></dd>
	<dt>comment</dt><dd><code>string</code>&nbsp;<span class="required">(required)</span>&nbsp;<span class="text-muted example"><strong>Example:&nbsp;</strong><span>Vel voluptates eaque qui...</span></span><p>Comment body</p></dd>
</dl>
</li></ul><ul class="list-group"><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>201</code></strong><a data-toggle="collapse" data-target="#46eda762afe7d9a346da8b598c70734c" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="46eda762afe7d9a346da8b598c70734c" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Cache-Control</span>: <span class="hljs-string">no-cache</span><br><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>    [
        {
                            "<span class="hljs-attribute">comment_id</span>": <span class="hljs-value"><span class="hljs-number">51</span>
                        </span>}
    ]            
</code></pre></li><li class="list-group-item"><strong>Response&nbsp;&nbsp;<code>400</code></strong><a data-toggle="collapse" data-target="#98d7991ed21fe7db8eabe421a48ce714" class="pull-right collapsed"><span class="closed">Show</span><span class="open">Hide</span></a></li><li id="98d7991ed21fe7db8eabe421a48ce714" class="list-group-item panel-collapse collapse"><div class="description"></div><h5>Headers</h5><pre><code><span class="hljs-attribute">Content-Type</span>: <span class="hljs-string">application/json</span><br></code></pre><h5>Body</h5><pre><code>{
  "<span class="hljs-attribute">error</span>": <span class="hljs-value">{
    "<span class="hljs-attribute">http_code</span>": <span class="hljs-value"><span class="hljs-number">400</span></span>,
    "<span class="hljs-attribute">message</span>": <span class="hljs-value">{
      "<span class="hljs-attribute">title</span>": <span class="hljs-value">[
        <span class="hljs-string">"The missing field is required."</span>
      ]
    </span>}
  </span>}
</span>}
</code></pre></li></ul></section></div></div></div><div><div class="panel panel-default"><div class="panel-heading"><h3 id="changelog">Changelog&nbsp;<a href="#changelog"><i class="fa fa-link"></i></a></h3></div><div class="panel-body"><h4 id="changelog-document-v1.0">Document v1.0&nbsp;<a href="#changelog-document-v1.0"><i class="fa fa-link"></i></a></h4><p>2014/10/13 - Initial release</p>
</div></div></div></div></div></div><p style="text-align: center;" class="text-muted">Generated by&nbsp;<a href="https://github.com/danielgtaylor/aglio" class="aglio">aglio</a>&nbsp;on 15 Oct 2014</p><div id="localFile" style="display: none; position: absolute; top: 0; left: 0; width: 100%; color: white; background: red; font-size: 150%; text-align: center; padding: 1em;">This page may not display correctly when opened as a local file. Instead, view it from a web server.

</div></body><script src="//code.jquery.com/jquery-1.11.0.min.js"></script><script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script><script>(function() {
  if (location.protocol === 'file:') {
    document.getElementById('localFile').style.display = 'block';
  }

}).call(this);
</script><script>(function() {
  $('table').addClass('table');

}).call(this);
</script></html>