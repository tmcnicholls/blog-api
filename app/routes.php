<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
    return '';
});

Route::group(array('prefix' => 'v1', 'before' => ''), function() 
{
	Route::get('/docs', function()
	{
		return View::make('docs');
	});
	Route::resource('posts', 'PostController', array('only' => array('index', 'store')) );
	Route::resource('comments', 'CommentController', array('only' => array('index', 'store')) );	
});